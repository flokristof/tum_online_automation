import telegram_
import threading
import dailycheck


def main():
    dailychecker = threading.Thread(target=dailycheck.run)
    dailychecker.start()
    telegram_.main_bot()

if __name__ == '__main__':
    main()
