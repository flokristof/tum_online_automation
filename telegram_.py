import os
from textwrap import dedent

import telegram
from telegram import ChatAction
from telegram.ext import (
    CommandHandler,
    Filters,
    MessageHandler,
    Updater,
    CallbackQueryHandler,
)
from telegram.utils import helpers

import moodle_integration
import time

from import_config import config

# Globals
admin_user = None

token = config.telegram_token
updater = Updater(token=token, use_context=True)


def send_text(text):
    context = updater
    context.bot.send_message(
        chat_id=config.telegram_user_id,
        text=text,
        parse_mode="Markdown",
        disable_web_page_preview=True,
    )


def start(update, context):

    username = update.effective_user.first_name
    message = f"""
    Hi *{username}*,
    I am a bot to help students to automate stuff.
    
    Commands:
    /newstuff - Search Moodle for new stuff!"""
    send_text(message)


def newstuff(update, context):
    moodle_integration.moodle_integration()


def courses(update, context):
    message = "You are inscribed in the following courses:\n"
    for course in config.courses:
        message += f"*{course.name}*" + "\n"
    send_text(message)


def main_bot():

    dp = updater.dispatcher

    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("newstuff", newstuff))
    dp.add_handler(CommandHandler("courses", courses))

    while True:
        try:
            updater.start_polling()
            updater.idle()
        except Exception:
            time.sleep(15)
