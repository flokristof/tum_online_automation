import schedule
import time
import moodle_integration
from import_config import config


def dailychecker():
    moodle_integration.moodle_integration()


def run():
    for one_time in config.dailycheck_time:
        schedule.every().day.at(one_time).do(dailychecker)
    while(1):
        schedule.run_pending()
        time.sleep(60)