
from selenium import webdriver
import downloadwait
import os
import shutil
from import_config import config

import telegram_


def moodle_integration():
    moodle_text = "Moodle search started:"
    telegram_.send_text(moodle_text)
    print(moodle_text)
    download_folder = config.temporary_download_folder
    prefs = {"plugins.always_open_pdf_externally": True,
                "download.default_directory": download_folder, "download.extensions_to_open": "",
                "download.prompt_for_download": False, "download.directory_upgrade": True,
                "safebrowsing_for_trusted_sources_enabled": False, "safebrowsing.enabled": False}
    chromeOptions = webdriver.ChromeOptions()
    chromeOptions.add_experimental_option("prefs", prefs)
    chromedriver = config.chromedriver_folder
    driver = webdriver.Chrome(executable_path=chromedriver, options=chromeOptions)

    driver.get("https://www.moodle.tum.de/login/index.php")
    driver.find_element_by_link_text("TUM LOGIN").click()

    username = driver.find_element_by_id("username")
    password = driver.find_element_by_id("password")

    username.send_keys(config.moodle_username)
    password.send_keys(config.moodle_password)
    driver.find_element_by_id("btnLogin").click()

    chosencourses = config.courses

    something_new = 0

    for course in chosencourses:

        # open Document for the first time
        resourcetxt = open("resource" + str(course.courseid) + ".txt", "a+")
        resourcetxt.close()

        # creating sets and add start link
        startlink = str("https://www.moodle.tum.de/course/view.php?id=" + str(course.courseid))

        resourcelinks = set()
        pages = set()
        videos = set()
        newvideos = set()

        pagekeywords = ["/assign/", "/folder/", "section="]
        resourcekeywords = [".pdf", ".zip", ".7z", "/resource/"]
        videokeywords = ["/mod/url/view", "/Panopto/Pages/Embed.", "/Panopto/Pages/Viewer."]
        driver.get(startlink)

        # checking first page
        mainlinks = driver.find_elements_by_css_selector('a')
        for mainlink in mainlinks:
            strlink = str(mainlink.get_attribute('href'))
            if any(x in strlink for x in resourcekeywords):
                resourcelinks.add(strlink)
            if any(x in strlink for x in videokeywords):
                videos.add(strlink)
            if any(x in strlink for x in pagekeywords):
                pages.add(strlink)

        # checking pages
        for page in pages:
            driver.get(page)
            links = driver.find_elements_by_css_selector('a')
            for link in links:
                strlink = str(link.get_attribute('href'))
                if any(x in strlink for x in resourcekeywords):
                    resourcelinks.add(strlink)
                if any(x in strlink for x in videokeywords):
                    videos.add(strlink)

        # download and store all new files
        for resourcelink in resourcelinks:
            resourceread = open("resource" + str(course.courseid) + ".txt", "r+").read()

            if resourcelink not in resourceread:
                resourcetxt = open("resource"+str(course.courseid)+".txt", "a")
                resourcetxt.write(resourcelink+"\n")
                resourcetxt.close()

                driver.get(resourcelink)
                downloadwait.download_wait(download_folder)

        # check if videos are new
        for video in videos:
            resourceread = open("resource" + str(course.courseid) + ".txt", "r+").read()
            if video not in resourceread:
                newvideos.add(video)
                resourcetxt = open("resource" + str(course.courseid) + ".txt", "a")
                resourcetxt.write(video + "\n")
                resourcetxt.close()

        # move new files to appropriate directory
        files = os.listdir(download_folder)
        oldfiles = os.listdir(course.folder)
        if files or newvideos:
            something_new = 1
            message = f"*{course.name}*:\n"
            if newvideos:
                lenofnewvideos = len(newvideos)
                if lenofnewvideos == 1:
                    message += str(len(newvideos)) + " new video available!\n"
                else:
                    message += str(len(newvideos)) + " new videos available!\n"
            if files:
                for f in files:
                    if f in oldfiles:
                        os.remove(os.path.join(download_folder, f))
                    else:
                        message += f"\t*{f}*\n"
                        shutil.move(os.path.join(download_folder, f), os.path.join(course.folder, f))
            telegram_.send_text(message)
            print(message)

    # check if something happened or nothing new was uploaded
    if something_new:
        searchfinished = "Moodle search finished!"
        print(searchfinished)
        telegram_.send_text(searchfinished)
    else:
        nothingnew = "There is nothing new!"
        print(nothingnew)
        telegram_.send_text(nothingnew)
    driver.quit()
