import json
from types import SimpleNamespace

config = json.load(open('./config.json'), object_hook=lambda d: SimpleNamespace(**d))